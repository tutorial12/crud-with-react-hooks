import React, { useState } from 'react';
import UserTable from './tables/UserTable';
import AddUserForm from './forms/AddUserForm';
import EditUserForm from './forms/EditUserForm';

function App() {
  const usersData = [
    {id:1,name:'Dono',username:'don321'},
    {id:2,name:'Kasino',username:'kas123'},
    {id:3,name:'Indro',username:'inasd123'},
  ];
  const initialFormState = {id:null,name:'',username:''};

  const [users,setUsers] = useState(usersData);
  const [editing,setEditing] = useState(false);
  const [currentUser,setCurrentUser] = useState(initialFormState);

  const addUser = (user) => {
    user.id = users.length + 1;
    setUsers([...users,user]);
  };

  const deleteUser = (id)=> {
    setEditing(false);
    setUsers(users.filter((user) => user.id !== id));
  };

  const updateUser = (id, updatedUser) => {
    setEditing(false);
    setUsers(users.map((user)=>(user.id === id ? updatedUser:user)));
  }

  const editRow = (user) => {
    setEditing(true);
    setCurrentUser({id:user.id,name:user.name,username:user.username});
  }

  return (
    <div className="container">
      <h1>CRUD App with Hooks</h1>
      <div className="flex-row">
        <div className="flex-large">
          {editing ? (
            <div>
              <h2>Edit user</h2>
              <EditUserForm
                setEditing={setEditing}
                currentUser={currentUser}
                updateUser={updateUser}
              />
            </div>
          ):(
          <div>
            <h2>Add user</h2>
            <AddUserForm AddUser={addUser}/>
            </div>)}
          </div>
        <div className="flex-large">
          <h2>View Users</h2>
          <UserTable users={users} deleteUser={deleteUser} editRow={editRow}/>
        </div>
      </div>
    </div>
  );
}

export default App;
