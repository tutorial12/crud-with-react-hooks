import React, {useState} from 'react';

const AddUserForm = (props) => {
    const initialFormState = {id:null,name:'',username:''};
    const [user,setUser] = useState(initialFormState);

    const handleInputChange = (event)=>{
        const {name,value} = event.target;
        setUser({...user,[name]:value});
    };

    const handleFormSubmit = (event) => {
        event.preventDefault();
        if(!user.name||!user.username) return
        console.log(props.AddUser);
        props.AddUser(user);
        setUser(initialFormState);
    };

    return (
        <form onSubmit={handleFormSubmit}>
            <label>Name</label>
            <input type="text" name="name" value={user.name} onChange={handleInputChange}/>
            <label>Username</label>
            <input type="text" name="username" value={user.username} onChange={handleInputChange}/>
            <button>Add New User</button>
        </form>
    )
}

export default AddUserForm;